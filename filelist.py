#!/usr/bin/env python3
'''filelist.py - tool for captioning audio clips for Tacotron2 training'''
import os
import glob
import shutil
import threading
from playsound import playsound

__version__ = '1.0'

DEFAULT_FILELIST  = 'filelist.txt'
DEFAULT_EXT       = 'wav'
DEFAULT_DELIMITER = '|'

def playsound_async(filename):
    '''start playsound in a thread'''
    thread = threading.Thread(target=playsound, args=(filename,))
    thread.start()
    return thread

def save_filelist(file_cues, path_prefix=None,
                  filename=DEFAULT_FILELIST,
                  delimiter=DEFAULT_DELIMITER):
    if path_prefix is None:
        path_prefix = os.path.basename(os.path.abspath(os.curdir))
    f = open(filename, 'w')
    for filename, cue in file_cues:
        f.write('{}{}{}\n'.format(os.path.join(path_prefix, filename), delimiter, cue))

def print_help():
    print("? - replay an audio clip")
    print("! - redo the last line")
    print("> - use the filename as the line, punctuation can be added with >, >. >! >?")
    print("To skip a line, leave the input blank and press enter.")

def main(filelist, target_directory, path_prefix=None,
         ext=DEFAULT_EXT,
         delimiter=DEFAULT_DELIMITER):
    # check that a filelist doesn't already exist
    if os.path.exists(filelist):
        print('{} already exists, using listed filenames'.format(filelist))
        file_cues = []
        for line in open(filelist).read().split('\n'):
            if line.find('|') == -1: continue
            filename, cue = line.split('|')
            file_cues.append([os.path.basename(filename), cue])
        shutil.move(filelist, '{}.bak'.format(filelist))
    else:
        # create new filelist
        file_cues = glob.glob(os.path.join(target_directory, '*.{}'.format(ext)))
        file_cues.sort()
        file_cues = list(map(list, zip([os.path.basename(filename) for filename in file_cues], [''] * len(file_cues))))
        if path_prefix is None:
            path_prefix = target_directory
    save_filelist(file_cues, path_prefix, filelist, delimiter) # update in case the user is changing the prefix
    
    # decend through filelist to edit cues
    print_help()
    i = 0
    while i < len(file_cues):
        filename, cue = file_cues[i]
        full_filename = os.path.join(target_directory, filename)
        if os.path.exists(full_filename):
            playsound_async(full_filename)
        else:
            print('{} not found'.format(full_filename))
            continue
        while True:
            if cue != '': new_cue = input('[{}] {}:'.format(full_filename, cue))
            else: new_cue = input('[{}]:'.format(full_filename))
            if new_cue == '': # skip line
                break
            elif new_cue == '?': # replay audio clip
                playsound_async(full_filename)
            elif new_cue == '!': # redo last line
                if i > 0: i -= 2
                else: i -= 1
                break
            elif new_cue[0] == '>':
                file_cues[i][1] = filename[filename.find('-')+1:filename.rfind('.')] + new_cue[1:]
                save_filelist(file_cues, path_prefix, filelist, delimiter)
                break
            else: # update line
                file_cues[i][1] = new_cue
                save_filelist(file_cues, path_prefix, filelist, delimiter)
                break
        i += 1

if __name__ == '__main__':
    from argparse import ArgumentParser
    p = ArgumentParser()
    p.add_argument('--path-prefix', '-p',
        type=str, help='path prefix to wave files')
    p.add_argument('--target-directory', '-t', default="output",
        type=str, help='directory containing audio clips to caption')
    p.add_argument('--filelist', '-f', default=DEFAULT_FILELIST,
        type=str, help='filename of filelist, default: {}'.format(
            DEFAULT_FILELIST))
    p.add_argument('--ext', '-e', default=DEFAULT_EXT,
        type=str, help='audio clip extension, default: {}'.format(
            DEFAULT_EXT))
    p.add_argument('--delimiter', '-d', default=DEFAULT_DELIMITER,
        type=str, help='delimiter character, default: {}'.format(
            DEFAULT_DELIMITER))
    args = p.parse_args()
    main(args.filelist, args.target_directory,
         args.path_prefix, args.ext, args.delimiter)
