'''test.py - run some tests to make sure everything is working'''
import sys
import subprocess
import shlex

if sys.platform == "linux": # terminal colors
    C_RED = "\033[91m"
    C_GREEN = "\033[92m"
    C_ENDC = "\033[0m"
else: # windows doesn"t support colored terminal
    C_RED = ""
    C_GREEN = ""
    C_ENDC = ""

def run(cmd, stdout=None):
    return_code = subprocess.call(cmd, stdout=stdout)
    return return_code

def run_tests(cmds, stdout=None):
    passing = 0
    failing = 0
    for cmd in cmds:
        return_code = run(cmd, stdout)
        if return_code == 0:
            print("[" + C_GREEN + "PASS" + C_ENDC + "] " + " ".join(cmd))
            passing += 1
        else:
            print("[" + C_RED + "FAIL" + C_ENDC + "] error code {}: ".format(return_code) + " ".join(cmd))
            failing += 1
    print("Passing: {}".format(passing))
    print("Failing: {}".format(failing))
    print("  Total: {}".format(passing + failing))

from argparse import ArgumentParser
p = ArgumentParser()
p.add_argument("command_file", type=str,
               help="bash script of test commands to run")
args = p.parse_args()

cmds = []
for line in open(args.command_file):
    if line.strip() == '' or line[0] == '#': continue
    cmds.append( shlex.split( line.strip() ) )
        
run_tests(cmds)