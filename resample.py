#!/usr/bin/env python3
'''resample.py - script to quickly resample audio clips'''
import os
import sys
import shutil
import glob
import subprocess

__version__ = '1.0'

DEFAULT_SAMPLE_RATE=22050
DEFAULT_CHANNELS=1
DEFAULT_PREFIX = 'r'
DEFAULT_EXT = 'wav'
DEFAULT_AVTOOL = 'ffmpeg'

def resample(config):
    files = glob.glob(os.path.join(config.target_directory, '*.{}'.format(config.ext)))
    for filename in files:
        if config.prefix == '':
            if sys.platform == 'linux':
                output_filename = os.path.join("/tmp", os.path.basename(filename))
            else:
                if not os.path.exists('tmp'):
                    os.mkdir('tmp')
                output_filename = os.path.join("tmp", os.path.basename(filename))
        else:
            output_filename = os.path.join(config.target_directory, config.prefix + os.path.basename(filename))
        cmd = [
            config.avtool,
            '-loglevel', config.loglevel,
            '-i', filename,
            '-ar', str(config.sample_rate),
            '-ac', str(config.channels),
            '-y',
            output_filename
        ]
        ret = subprocess.call(cmd)
        if ret != 0:
            print('Error: status code {}'.format(ret))
            raise SystemExit
        if config.prefix == '':
            os.remove(filename)
            shutil.move(output_filename, filename)

if __name__ == '__main__':
    from argparse import ArgumentParser
    p = ArgumentParser()
    p.add_argument('--target-directory', '-t',
        type=str, help='directory of audio clips to resample')
    p.add_argument('--sample-rate', '-r', default=DEFAULT_SAMPLE_RATE,
        type=int, help='sample rate to resample audio clips to')
    p.add_argument('--channels', '-c', default=DEFAULT_CHANNELS,
        type=int, help='number of channels to resample audio clips to')
    p.add_argument('--prefix', '-p', default=DEFAULT_PREFIX,
        type=str, help='filename prefix of resampled audio clips')
    p.add_argument('--ext', '-e', default=DEFAULT_EXT,
        type=str, help='audio clip extension to resample')
    p.add_argument('--avtool', '-t', default=DEFAULT_AVTOOL,
        type=str, help='audio/video tool to use')
    args = p.parse_args()
    resample(args)
    
