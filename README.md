# Clipchan v0.3.0

Automatically create audio clips from anime, shows and movies using subtitles and denoise and process them for machine learning

## To-Do

- ✅ Robustify against bad inputs
- ✅ Add inspect option to see what styles the subtitles use, see `--inspect / -i`
- ✅ Extract subtitles directly from videos
- ✅ Output clip filelist with subtitles to use with machine learning
- ✅ Automatically denoise audio clips with Spleeter
- ✅ Automate clip preparation
- Automate Spleeter setup
- Support other audio formats besides Advanced SubStation Alpha format
- Automatically fix misaligned subtitles
- Speaker identification
- GUI interface

## Requirements

- Python 3, (Spleeter requires 3.5-3.7)
- FFmpeg

## Setup

```
git clone https://gitlab.com/kokubunji/clipchan.git
cd clipchan
pip install -r requirements.txt
```

## Subtitle preparation

Use [Aegisub](http://www.aegisub.org/) to re-align subtitles with a video before using Clipchan to clip them, otherwise you will have to manually edit them by hand. Generally Clipchan will do a good job clipping audio so long as there is a pause between two lines and no background noise.

## Example

```
python clipchan.py 01.mp4 --subtitles 01.ass --style JPDB --spleeter --auto-clean
```
### Most useful options

`--subtitles` use a subtitle file instead of the subtitles found in the video.

`--style` selects subtitles to clip only with that subtitle style.

`--spleeter` removes background audio and noise.

`--auto-clean` normalizes audio clips and removes silence from the beginnings and ends.

`--padding` helps clip messy subs but may clip unwanted audio if it is too large, defaults to 0.2 seconds.

 Before clipping audio you can run Clipchan with `--inspect` to see the most common styles used in the subtitles file.

## Usage

```
usage: clipchan.py [-h] [--version] [--subtitles SUBTITLES] [--use-libzvbi]
                   [--inspect] [--style STYLE] [--padding PADDING]
                   [--scale SCALE] [--encoding ENCODING] [--avtool AVTOOL]
                   [--output-path OUTPUT_PATH] [--filelist FILELIST]
                   [--filelist-output-path FILELIST_OUTPUT_PATH] [--ext EXT]
                   [--no-clobber] [--debug] [--guess] [--track TRACK]
                   [--quiet] [--subtitle-stream SUBTITLE_STREAM] [--spleeter]
                   [--spleeter-model SPLEETER_MODEL] [--channels {1,2}]
                   [--sample-rate SAMPLE_RATE] [--auto-clean]
                   [--target-amplitude TARGET_AMPLITUDE]
                   [--clip-crystalization CLIP_CRYSTALIZATION]
                   [--clip-threshold-start CLIP_THRESHOLD_START]
                   [--clip-threshold-end CLIP_THRESHOLD_END]
                   [--clip-chunk-duration CLIP_CHUNK_DURATION]
                   [--clip-area CLIP_AREA]
                   [--clip-trailing-pad CLIP_TRAILING_PAD]
                   video_file

positional arguments:
  video_file            video file to clip

optional arguments:
  -h, --help            show this help message and exit
  --version             print version information and exit
  --subtitles SUBTITLES, -S SUBTITLES
                        ignore video subtitles and use a different subtitles
                        file
  --use-libzvbi, -z     use libzvbi to extract subtitles, faster but not
                        supported by all distros
  --inspect, -i         inspect subtitle file to find the most common event
                        style
  --style STYLE, -s STYLE
                        only clip subtiles of a certain style
  --padding PADDING, -p PADDING
                        audio clip padding to prevent missing audio
  --scale SCALE         time scaling, for slowed down or sped up videos
  --encoding ENCODING   subtitle encoding
  --avtool AVTOOL       audio/video tool
  --output-path OUTPUT_PATH, -o OUTPUT_PATH
                        output path of audio clips
  --filelist FILELIST, -f FILELIST
                        output filename of generated filelist, relative to
                        output path
  --filelist-output-path FILELIST_OUTPUT_PATH, -l FILELIST_OUTPUT_PATH
                        output path of filelist
  --ext EXT, -e EXT     audio extension
  --no-clobber, -n      don't overwrite audio clips
  --debug, -D           outputs clip processing info to clipchan.log
  --guess, -g           extract audio clips only for the most common subtitle
                        style
  --track TRACK, -t TRACK
                        extract a different audio track
  --quiet, -q           quiet ffmpeg output, only show fatal errors
  --subtitle-stream SUBTITLE_STREAM, -b SUBTITLE_STREAM
                        subtitle stream to use, usually 3 or 4 if two audio
                        tracks
  --spleeter, -d        use spleeter to denoise audio clips
  --spleeter-model SPLEETER_MODEL, -m SPLEETER_MODEL
                        spleeter model to use
  --channels {1,2}, -c {1,2}
                        audio channels to resample post processing
  --sample-rate SAMPLE_RATE, -r SAMPLE_RATE
                        audio channels to resample post processing
  --auto-clean, -a      automatically remove silence and normalize audio clips
  --target-amplitude TARGET_AMPLITUDE
                        amplitude to normalize to, 0.5 is -6 DB
  --clip-crystalization CLIP_CRYSTALIZATION
                        increase clarity
  --clip-threshold-start CLIP_THRESHOLD_START
                        clipping threshold amplitude at start of audio clip
  --clip-threshold-end CLIP_THRESHOLD_END
                        clipping threshold amplitude at end of audio clip
  --clip-chunk-duration CLIP_CHUNK_DURATION
                        clipping chunk search size in seconds
  --clip-area CLIP_AREA
                        clipping area to search at start and end of clip in
                        seconds
  --clip-trailing-pad CLIP_TRAILING_PAD
                        duration to keep after clip in seconds
```

## Remove music from audio clips

### Setup Spleeter

Quick setup:
```
bash spleeter.sh
sudo python setup.py install
```
Note: Spleeter requires Python 3.5-3.7

#### Manually download pretrained model

From within Clipchan's directory:
```
wget https://github.com/deezer/spleeter/releases/download/v1.4.0/2stems.tar.gz
mkdir -p pretrained_models/2stems
tar -C pretrained_models/2stems -xvf 2stems.tar.gz
```

#### Manually download Spleeter
```
wget https://github.com/deezer/spleeter/archive/v1.4.0.tar.gz
tar xvf v1.4.0.tar.gz
cd spleeter-1.4.0
sudo python setup.py install
```

### Usage

Clipchan can now remove background music and noise with Spleeter by using the `--spleeter` option.

Or to use Spleeter manually:

⚠️ Note: do not use it on clips created with the copy codec or the output of Spleeter will behave unexpectedly.

```
spleeter separate -i audio_example.wav -o audio_output
```

See the [Spleeter wiki](https://github.com/deezer/spleeter/wiki/2.-Getting-started) for more usage instructions.

⚠️ Spleeter will look for pretrained models in the current directory and redownload them if not found. Make sure to use it from the same directory as Clipchan.

## Remove noise from audio clips

[DTLN](https://github.com/breizhn/DTLN) is not built into Clipchan yet but is also good at removing heavy noise and sound effects.

### Setup
```
git clone https://github.com/breizhn/DTLN.git
mkdir input # put your audio clips here
mkdir output
```

⚠️ DTLN requires audio clips to be 16 KHz WAV files. It will not work smoothly at other sampling rates.

### Usage
```
python DTLN/run_evaluation.py -i ./input -o ./output -m DTLN/pretrained_model/model.h5
```

## Preparing clips for machine learning

- Check audio clips and crop out unwanted audio and silence. Clipchan's `--auto-clean` will take care of most of this.
- Filter clips with background noise and music with [Spleeter](https://github.com/deezer/spleeter) (best for music removal) or [DTLN](https://github.com/breizhn/DTLN) (best for sound effects and noise). Spleeter can be automatically applied to clips made within Clipchan with `--spleeter`.
- Normalize audio clips to -6 DB. Clipchan's `--auto-clean` feature will do this for you.

## Tacotron2
- Make sure audio clips are 16-bit 22050 Hz mono-channel WAV files. Clipchan's default settings will do this automatically. Or if desired, a fresh model can be trained in Tacotron2 with a different sampling rate. Then you will need to change Clipchan's output with `--sample-rate`.
- Split roughly 1% into a validation set, and 99% into a training set. [WaifuSynth] provides `split_filelist.py` to do this.
- Create pipe separated filelists for each set, for example:
```
character_name/01-clip_filename.wav|Hello world!
character_name/02-clip_filename.wav|Do robots dream of electric sheep?
```
`filelist.py` provided with Clipchan can be used to caption audio clips and create a pipe separated filelist:
```
python filelist.py --target-directory output --filelist filelist.txt
```
Enter `?` to replay an audio clip. Enter `!` to redo the last line. To skip a line, leave the input blank and press enter. The filelist will save with each modification. At start up, if a filelist file already exists it will be moved to a `.bak` file.

[WaifuSynth]: https://gitlab.com/kokubunji/waifusynth
