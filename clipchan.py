#!/usr/bin/env python3
"""clipchan - automatically clip videos with subtitles"""
import os
import sys
import shutil
#import datetime
import subprocess
#import json
import re
import glob

#import resampy
import ass
import numpy as np
from numpy import abs, mean #, log10
from scipy.io.wavfile import read, write

__version__ = "0.3.0"

DEFAULT_TRACK = 1
DEFAULT_SUBTITLE_STREAM = 2
DEFAULT_FILELIST = "./filelist.txt"
DEFAULT_EXT = "wav"
DEFAULT_AVTOOL = "ffmpeg"
DEFAULT_LOGLEVEL = "warning"
DEFAULT_PADDING = 0.2
DEFAULT_ENCODING = "utf_8_sig"
#DEFAULT_RESAMPLE_RATE = 22050 # don't resample by default
DEFAULT_CHANNELS = 1
DEFAULT_TARGET_AMPLITUDE = 0.5
DEFAULT_CLIP_THRESHOLD_START = 0.0025
DEFAULT_CLIP_THRESHOLD_END = 0.02
DEFAULT_CLIP_CHUNK_DURATION = 0.02
DEFAULT_CLIP_AREA = 2.0
DEFAULT_CLIP_CRYSTALIZATION = 1
DEFAULT_CLIP_TRAILING_PAD = 0.06
DEFAULT_RESAMPLE_FILTER = 'kaiser_best'
DEFAULT_OUTPUT_PATH = 'output'
DEFAULT_FILELIST_OUTPUT_PATH = '.'
DEFAULT_SPLEETER_MODEL = "spleeter:2stems"

ERROR = 1
ARGUMENT_LIMIT = 1000

CLIPCHAN_LOG = "clipchan.log"
SPLEETER_LOG = "spleeter.log"
WINDOWS_TEMP_SUBTITLES = "clipchan.ass"
WAV_MAX = 32767

if sys.platform == "linux": # terminal colors
    C_RED = "\033[91m"
    C_YELLOW = "\033[93m"
    C_GREEN = "\033[92m"
    C_BLUE = "\033[94m"
    C_BOLD = "\033[1m"
    C_ENDC = "\033[0m"
else: # windows doesn"t support colored terminal
    C_RED = ""
    C_YELLOW = ""
    C_GREEN = ""
    C_BLUE = ""
    C_BOLD = ""
    C_ENDC = ""

class ClipchanConfig:
    def __init__(self, args):
        if type(args) == dict:
            self.__dict__ = args
        else:
            self.__dict__ = args.__dict__

def error(message):
    return C_RED + "Error: {}".format(message) + C_ENDC

def warning(message):
    return C_YELLOW + "Warning: {}".format(message) + C_ENDC

def notice(message):
    return C_GREEN + "{}".format(message) + C_ENDC

def debug(message):
    return C_BLUE + "{}".format(message) + C_ENDC

def dt2s(t):
    """timedelta to seconds"""
    return t.seconds + t.microseconds / 1000000.0

def time_ass2hms(t, offset=0.0, scale=1.0):
    """converts datetime.timedelta to H:M:S timestamp"""
    s = dt2s(t)
    s *= scale
    s += offset
    m = int(s // 60)
    s -= m * 60
    h = int(m // 60)
    m -= h * 60
    return "{:02}:{:02}:{:02.3f}".format(h, m, s)

def inspect(subtitles, encoding="utf_8_sig"):
    from collections import Counter
    if not os.path.exists(subtitles):
        print(error("subtitles file not found"))
        sys.exit(ERROR)
    with open(subtitles, encoding=encoding) as f:
        doc = ass.parse(f)
    counter = Counter()
    for event in doc.events:
        counter[event.style] += 1
    return counter.most_common()[::-1]

def inspect_print(subtiles, config):
    for style, count in inspect(subtitles, config.encoding):
        print("{:4} {}".format(count, style))

def extract_subtitles(config):
    if sys.platform == "linux":
        subtitles_filename = "/tmp/clipchan.ass"
    else:
        # make portable to Windows
        subtitles_filename = WINDOWS_TEMP_SUBTITLES
    if config.use_libzvbi: avtool_libzvbi = ["-txt_format", "text"]
    else: avtool_libzvbi = []
    cmd = [
        config.avtool,
        "-loglevel", config.loglevel
    ] + avtool_libzvbi + [
        "-an", "-vn",
        "-i", config.input_file,
        "-map", "0:{}".format(config.subtitle_stream),
        "-y",
        subtitles_filename
    ]
    return_code = subprocess.call(cmd)
    if config.debug: print(debug(" ".join(cmd)))
    if return_code != 0:
        if config.subtitle_stream < 4:
            hint = ", maybe try {s}? (use -b {s})".format(
                    s=max(2, config.subtitle_stream+1))
        else:
            hint = ""
        print(error("subtitles not found in stream #{}{}".format(
                    config.subtitle_stream, hint)))
        sys.exit(return_code)
    return subtitles_filename

def create_audio_clips(config):
    # TODO function too long, refactor
    if config.subtitles and not os.path.exists(config.subtitles):
        print(error("subtitles file not found"))
        sys.exit(ERROR)
    if config.subtitles is None or not os.path.exists(config.subtitles):
        config.subtitles = extract_subtitles(config)
    with open(config.subtitles, encoding=config.encoding) as f:
        doc = ass.parse(f)
    #offsets = json.loads(config.offsets)
    #offsets.sort()
    if config.no_clobber: avtool_clobber = "-n"
    else: avtool_clobber = "-y"
    if config.debug: log = open(CLIPCHAN_LOG, "w")
    filelist_path = os.path.join(config.filelist_output_path, config.filelist)
    if os.path.exists(filelist_path) and config.no_clobber:
        print(error("Filelist {} already exists, aborting.".format(filelist_path)))
        sys.exit(ERROR)
    if not os.path.exists(config.filelist_output_path):
        os.mkdir(config.filelist_output_path)
    filelist = open(filelist_path, "w")
    if config.guess:
        style = inspect(config.subtitles)[-1][0]
        if config.debug: print(debug("Guessing: {}".format(style)))
    else: style = config.style
    i = 0
    if not os.path.exists(config.output_path):
        os.mkdir(config.output_path)
    for event in doc.events:
        if style is not None:
            if event.style != style: continue
        text = re.sub("{[^}]+}", "", event.text) # clean curly brace formatting
        text = text.replace("\\N", " ") # blank new lines
        text = re.sub(r"[\<\>\:\'\"\\\|\*\/\?\!\,\.]", "", text) # forbidden
        filename = "{:04}-{}".format(i, text)[:254-len(config.ext)] # limit filename length        
        full_filename = os.path.join(config.output_path, filename)
        filelist.write("{}.wav|{}\n".format(full_filename, event.text)) # write to filelist
        #event_time = dt2s(event.start)
        #for offset_time, _offset in offsets:
        #    if offset_time > event_time:
        #        break
        #    offset = _offset
        #start = time_ass2hms(event.start, offset-config.padding, config.scale)
        #end = time_ass2hms(event.end, offset+config.padding, config.scale)
        start = time_ass2hms(event.start, -config.padding, config.scale)
        end = time_ass2hms(event.end, config.padding, config.scale)
        if config.debug:
            log.write("[{}] {}-{} {} <> {}\n".format(
                i, start, end, text, event.text))
        if config.track is None:
            if config.input_file[config.input_file.rfind(".")+1:].lower() in ["aac", "m4a", "mp3", "wav", "ogg", "opus"]:
                track = "0"   
            else:
                track = "1"
        else:
            track = config.track
        cmd = [
            config.avtool,
            "-loglevel", config.loglevel,
            "-ss", "{}".format(start),
            "-to", "{}".format(end),
            "-i", config.input_file,
            "-map", "0:{}".format(track),
            "-vn", # no video
            avtool_clobber,
            "{}.{}".format(full_filename, config.ext)
        ]
        return_code = subprocess.call(cmd)
        if config.debug: print(debug(" ".join(cmd)))
        i += 1
        if return_code != 0:
            print(error("return code {} while converting {}".format(return_code, text)))
            sys.exit(return_code)
    print(notice("{} audio clips extracted to {}".format(i, config.output_path)))

def run_spleeter(config):
    # TODO refactor
    # warning: running spleeter from an interactive console
    #          will freeze the terminal
    if sys.platform == "linux":
        tmp_path = "/tmp/"
    else:
        tmp_path = "."
    
    clip_files = glob.glob(os.path.join(config.output_path, "*.{}".format(config.ext)))
    clip_files.sort()
    if config.ext == "m4a":
        print(notice("Converting m4a to wav before processing..."))
        new_clip_files = []
        for clip_filename in clip_files:
            wav_filename = clip_filename[:clip_filename.rfind(".")] + ".wav"
            if config.no_clobber: avtool_clobber = "-n"
            else: avtool_clobber = "-y"
            #if config.sample_rate:
            #    avtool_sample_rate = ["-ar", str(config.sample_rate)]
            #else: avtool_sample_rate = []
            #if config.sample_rate:
            #    avtool_channels = ["-ac", str(config.channels)]
            #else: avtool_channels = []
            cmd = [
                config.avtool,
                "-loglevel", config.loglevel,
                "-i", clip_filename,
            #] + avtool_sample_rate + avtool_channels + [
                avtool_clobber,
                wav_filename
            ]
            if config.debug: print(debug(" ".join(cmd)))
            return_code = subprocess.call(cmd)
            if return_code != 0:
                print(error("return code {} while converting {}".format(
                    return_code, clip_filename)))
                sys.exit(return_code)
            new_clip_files.append(wav_filename)
        clip_files = new_clip_files
    for i in range(0, len(clip_files), ARGUMENT_LIMIT):
        # work around argument list limit
        cmd = [
            "spleeter", "separate",
            "-o", tmp_path,
            "-p", config.spleeter_model,
            # BUG spleeter sometimes fails to produce output for first audio clip
            *([clip_files[i]] + clip_files[i:i+ARGUMENT_LIMIT])
        ]
        if config.debug: print(debug(" ".join(cmd)))
        # spleeter is noisy, redirecting stderr
        return_code = subprocess.call(cmd, stderr=open(SPLEETER_LOG, "w"))
        if return_code != 0:
            print(error("return code {} while running Spleeter, dumping spleeter.log\n\n{}".format(return_code, open("spleeter.log").read())))
            sys.exit(return_code)
        clip_files_chunk = clip_files[i:i+ARGUMENT_LIMIT]
        for clip_filename in clip_files_chunk:
            clip_filename_noext = clip_filename[:clip_filename.rfind(".")]
            output_filename = os.path.join(clip_filename_noext + ".wav")
            clip_basename_noext = os.path.basename(clip_filename_noext)
            spleeter_clip_filename = os.path.join(tmp_path, clip_basename_noext, "vocals.wav")
            if not os.path.exists(spleeter_clip_filename):
                print(warning("Spleeter failed to produce output for {}, skipping".format(spleeter_clip_filename)))
                continue
            if os.path.exists(output_filename):
                if config.debug:
                    print(debug("overwriting {}".format(output_filename)))
                    print(debug("rm " + output_filename))
                os.remove(output_filename)
            if config.debug: print(debug("mv " + spleeter_clip_filename + " " +  output_filename))
            shutil.move(spleeter_clip_filename, output_filename)
            accompaniment = os.path.join(tmp_path, clip_basename_noext, "accompaniment.wav")
            if os.path.exists(accompaniment):
                os.remove(accompaniment)
            if config.debug: print(debug("rm " + os.path.join(tmp_path, clip_basename_noext, "accompaniment.wav")))
            os.rmdir(os.path.join(tmp_path, clip_basename_noext))
            if config.debug: print(debug("rmdir " + os.path.join(tmp_path, clip_basename_noext)))
    #if not config.debug:
    #    subprocess.call(["reset"]) # XXX workaround for spleeter bug

def colorif(text, condition, true_color=C_GREEN, false_color=C_RED):
    if condition:
        return C_GREEN + text + C_ENDC
    return C_RED + text + C_ENDC

def clean_audio_clip(filename, config):
    padding_chunks = int(config.padding / config.clip_chunk_duration)
    if config.debug: print("{}: {}".format("padding_chunks", debug(str(padding_chunks))))
    clip_area_chunks = int(config.clip_area / config.clip_chunk_duration)
    if config.debug: print("{}: {}".format("clip_area_chunks", debug(str(clip_area_chunks))))
    clip_trailing_chunks = int(config.clip_trailing_pad / config.clip_chunk_duration)
    if config.debug: print("{}: {}".format("clip_trailing_chunks", debug(str(clip_trailing_chunks))))
    assert filename[filename.rfind(".")+1:] == "wav", error("clip cleaning only works with wavs")
    
    sample_rate, wav_data = read(filename)
    chunk_size = int(sample_rate / (1/config.clip_chunk_duration))
    if config.debug: print("{}: {}".format("chunk_size", debug(str(chunk_size))))
    num_chunks = wav_data.shape[0] // (chunk_size) 
    if config.debug: print("{}: {}".format("num_chunks", debug(str(num_chunks))))

    # normalize to target
    amplitude = max(-np.min(wav_data), np.max(wav_data))
    if config.debug: print("{}: {}".format("amplitude", debug(str(amplitude))))
    wav_data = wav_data * config.target_amplitude / amplitude
    wav_data = np.abs(wav_data)**(1/config.clip_crystalization) * np.sign(wav_data)
    wav_data = (wav_data * WAV_MAX).astype(np.int16)

    # clip silence
    num_chunks = wav_data.shape[0] // (chunk_size)
    chunks = np.array_split(wav_data, num_chunks)

    # find start
    silent_chunks = np.argwhere([mean(abs(chunk)) / WAV_MAX <= config.clip_threshold_start for chunk in chunks[:padding_chunks]])
    if config.debug: print(debug("silent_chunks {} [start {}] {}".format(
            len(silent_chunks),
            config.clip_threshold_start,
            ", ".join([colorif("{:.3f}".format(x), x <= config.clip_threshold_start) for x in [mean(abs(chunk)) / WAV_MAX for chunk in chunks[:padding_chunks]]]))))
    if len(silent_chunks) == 0:
        print(warning("no silence in beginning found, {}".format(filename)))
        start_chunk = 0
    else:
        start_chunk = int(np.median(silent_chunks))
    clip_start = [mean(abs(chunk)) / WAV_MAX > config.clip_threshold_start for chunk in chunks][start_chunk:clip_area_chunks]
    if config.debug: print(debug("clip_start {} [start {}] {}".format(
            len(clip_start),
            config.clip_threshold_start,
            ", ".join([colorif("{:.3f}".format(x), x > config.clip_threshold_start) for x in [mean(abs(chunk)) / WAV_MAX for chunk in chunks[start_chunk:clip_area_chunks]]]))))
    if True not in clip_start:
        print(warning("clip beginning not found, {}".format(filename)))
        start = 0
    else:
        start = start_chunk + clip_start.index(True)
    if config.debug: print("{}: {}".format("start", debug(str(start))))

    # find end
    silent_chunks = np.argwhere([mean(abs(chunk)) / WAV_MAX <= \
                                 config.clip_threshold_end \
                                 for chunk in chunks[-padding_chunks:]])
    if config.debug: print(debug("silent_chunks {} [end {}] {}".format(
            len(silent_chunks),
            config.clip_threshold_end,
            ", ".join([colorif("{:.3f}".format(x), x <= \
                               config.clip_threshold_end) \
            for x in [mean(abs(chunk)) / WAV_MAX \
                      for chunk in chunks[:padding_chunks]]]))))
    if len(silent_chunks) == 0:
        print(warning("no silence in end found, {}".format(filename)))
        end_chunk = len(chunks)
    else:
        end_chunk = int(np.median(silent_chunks)) + \
                    len(chunks) - padding_chunks
    clip_end = [mean(abs(chunk)) / WAV_MAX > config.clip_threshold_end \
                for chunk in chunks][-clip_area_chunks:end_chunk]
    if config.debug:
        print(debug("clip_end {} [end {}] {}".format(
            len(clip_end),
            config.clip_threshold_end,
            ", ".join([colorif("{:.3f}".format(x), x > \
                               config.clip_threshold_end) \
            for x in [mean(abs(chunk)) / WAV_MAX \
                      for chunk in chunks[-clip_area_chunks:end_chunk]]]))))
    if True not in clip_end:
        print(warning("clip end not found, {}".format(filename)))
        end = len(chunks)
    else:
        end = end_chunk - clip_end[::-1].index(True) + clip_trailing_chunks
    if config.debug: print("{}: {}".format("end", debug(str(end))))
    if end <= start:
        print(error("invalid crop length, [{}:{}], in {}".format(start*chunk_size, end*chunk_size, filename)))
        sys.exit(2)
    # save
    if config.debug:
        print(debug("cropped to [{}:{}], {}".format(start*chunk_size, end*chunk_size, wav_data.shape[0])))
    if config.debug: print(debug("write {}".format(filename)))
    
    if config.resample_rate is not None:
        if config.debug: print(debug(f"Resampling {sample_rate} to {config.resample_rate} Hz"))
        avtool_resample_options = ["-ar", f"{config.resample_rate}"]
        #wav_data = resampy.resample(wav_data[start*chunk_size:end*chunk_size],
        #                 sample_rate,
        #                 config.resample_rate,
        #                 filter=config.resample_filter)
        #print(debug((len(wav_data[start*chunk_size:end*chunk_size]), len(wav_data))))
        #sample_rate = config.resample_rate
    else:
        avtool_resample_options = []
    # convert channels
    if config.channels is not None:
        avtool_channel_options = ["-ac", f"{config.channels}"]
    else:
        avtool_channel_options = []
    avtool_options = avtool_resample_options + avtool_channel_options
    
    if len(avtool_options) != 0:
        if sys.platform == "linux":
            tmp_path = "/tmp/"
        else:
            os.mkdir("tmp")
            tmp_path = "tmp"
        dirname = os.path.dirname(filename)
        basename = os.path.basename(filename)
        tmp_file = os.path.join(tmp_path, basename)
        write(tmp_file, sample_rate, wav_data[start*chunk_size:end*chunk_size])
        if config.no_clobber: avtool_clobber = "-n"
        else: avtool_clobber = "-y"
        cmd = [
            config.avtool,
            "-loglevel", config.loglevel,
            "-i", tmp_file,
            "-af", "aresample=resampler=swr",
            avtool_clobber,
        ] + avtool_options + [filename]
        return_code = subprocess.call(cmd)
        if config.debug: print(debug(" ".join(cmd)))
        os.remove(tmp_file)
        if sys.platform != "linux":
            os.rmdir("tmp")
    else:
        write(filename, sample_rate, wav_data[start*chunk_size:end*chunk_size])

def clean_audio_clips(config, ext='wav'):
    clip_files = glob.glob(os.path.join(config.output_path, "*.{}".format(ext)))
    clip_files.sort()
    for clip_filename in clip_files:
        clean_audio_clip(clip_filename, config)

if __name__ == "__main__":
    if "--version" in sys.argv:
        print("Clipchan v{}".format(__version__))
        raise SystemExit
    from argparse import ArgumentParser
    p = ArgumentParser()
    p.add_argument("--version", action="store_true",
        help="print version information and exit")
    p.add_argument("input_file",
        type=str, help="video/audio file to clip")
    p.add_argument("--subtitles", "-S",
        type=str, help="ignore video subtitles and use a different subtitles file")
    p.add_argument("--use-libzvbi", "-z", action="store_true",
        help="use libzvbi to extract subtitles, faster but not supported by all distros")
    p.add_argument("--inspect", "-i", action="store_true",
        help="inspect subtitle file to find the most common event style")
    p.add_argument("--style", "-s",
        type=str, help="only clip subtiles of a certain style")
    #p.add_argument("--offsets", default="[[0.0, 0.0]]",
    #    type=str, help="subtitle offsets in json format [[time, offset], [...]]")
    p.add_argument("--padding", "-p", default=DEFAULT_PADDING,
        type=float, help="audio clip padding to prevent missing audio")
    p.add_argument("--scale", default=1.0,
        type=float, help="time scaling, for slowed down or sped up videos")
    p.add_argument("--encoding", default=DEFAULT_ENCODING,
        type=str, help="subtitle encoding")
    p.add_argument("--avtool", default=DEFAULT_AVTOOL,
        type=str, help="audio/video tool")
    #p.add_argument("--codec", type=str, help="audio codec")
    p.add_argument("--output-path", "-o", default=DEFAULT_OUTPUT_PATH, type=str,
        help="output path of audio clips")
    p.add_argument("--filelist", "-f", default=DEFAULT_FILELIST,
        type=str, help="output filename of generated filelist, relative to output path")
    p.add_argument("--filelist-output-path", "-l", default=DEFAULT_FILELIST_OUTPUT_PATH, type=str,
        help="output path of filelist")
    p.add_argument("--ext", "-e", default=DEFAULT_EXT,
        type=str, help="audio extension")
    p.add_argument("--no-clobber", "-n", action="store_true",
        help="don't overwrite audio clips")
    p.add_argument("--debug", "-D", action="store_true",
        help="outputs clip processing info to clipchan.log")
    p.add_argument("--guess", "-g", action="store_true",
        help="extract audio clips only for the most common subtitle style")
    p.add_argument("--track", "-t", default=None,
        type=int, help="extract a different audio track")
    p.add_argument("--quiet", "-q", action="store_true",
        help="quiet ffmpeg output, only show fatal errors")
    p.add_argument("--subtitle-stream", "-b", default=DEFAULT_SUBTITLE_STREAM, type=int,
        help="subtitle stream to use, usually 3 or 4 if two audio tracks")
    # spleeter
    p.add_argument("--spleeter", "-d", action="store_true",
        help="use spleeter to denoise audio clips")
    p.add_argument("--spleeter-model", "-m", default=DEFAULT_SPLEETER_MODEL,
        type=str, help="spleeter model to use")
    # audio clip resampling
    p.add_argument("--channels", "-c", default=DEFAULT_CHANNELS, choices=[1,2],
        type=int, help="audio channels to resample post processing")
    p.add_argument("--resample-rate", "-r",
        type=int, help="audio channels to resample post processing")
    p.add_argument("--resample-filter", "-R", default=DEFAULT_RESAMPLE_FILTER,
        choices=["kaiser_best", "kaiser_fast", "sinc_window"],
        type=str, help=f"resample filter to use default: {DEFAULT_RESAMPLE_FILTER}")
    # clipping and normalization arguments
    p.add_argument("--auto-clean", "-a", action="store_true",
        help="automatically remove silence and normalize audio clips")
    p.add_argument("--target-amplitude", default=DEFAULT_TARGET_AMPLITUDE,
        type=float, help="amplitude to normalize to, 0.5 is -6 DB")
    p.add_argument("--clip-crystalization", default=DEFAULT_CLIP_CRYSTALIZATION,
        type=float, help="increase clarity")
    p.add_argument("--clip-threshold-start", default=DEFAULT_CLIP_THRESHOLD_START,
        type=float, help="clipping threshold amplitude at start of audio clip")
    p.add_argument("--clip-threshold-end", default=DEFAULT_CLIP_THRESHOLD_END,
        type=float, help="clipping threshold amplitude at end of audio clip")    
    p.add_argument("--clip-chunk-duration", default=DEFAULT_CLIP_CHUNK_DURATION,
        type=float, help="clipping chunk search size in seconds")
    p.add_argument("--clip-area", default=DEFAULT_CLIP_AREA,
        type=float, help="clipping area to search at start and end of clip in seconds")
    p.add_argument("--clip-trailing-pad", default=DEFAULT_CLIP_TRAILING_PAD,
        type=float, help="duration to keep after clip in seconds")
    args = p.parse_args()
    config = ClipchanConfig(args)
    if args.debug:
        config.loglevel = "debug"
        print(debug(config.__dict__))
    else:
        config.loglevel = DEFAULT_LOGLEVEL
    if args.quiet: config.loglevel = "fatal"
    if args.inspect:
        if args.subtitles is None:
            subtitles = extract_subtitles(config)
        else:
            subtitles = args.subtitles
        inspect_print(subtitles, config)
        raise SystemExit
    create_audio_clips(args)
    if args.spleeter or args.spleeter_model:
        if args.spleeter_model == None:
            spleeter_model = DEFAULT_SPLEETER_MODEL
        else:
            spleeter_model = args.spleeter_model
        run_spleeter(config)
    if args.auto_clean:
        clean_audio_clips(config)
        resample_args = ClipchanConfig({
            "target_directory": args.output_path,
            "resample_rate": args.resample_rate,
            "channels": args.channels,
            "prefix": "",
            "ext": "wav",
            "avtool": "ffmpeg",
            "loglevel": config.loglevel
        })
    print("Success.")
