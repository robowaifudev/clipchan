#!
wget https://github.com/deezer/spleeter/releases/download/v1.4.0/2stems.tar.gz
mkdir -p pretrained_models/2stems
tar -C pretrained_models/2stems -xvf 2stems.tar.gz
wget https://github.com/deezer/spleeter/archive/v1.4.0.tar.gz
tar xvf v1.4.0.tar.gz
cd spleeter-1.4.0